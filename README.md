# Contexte du projet
Tu dois développer un système simplifié pour gérer les patients et le personnel médical d’un hôpital. Ce système devra permettre l’enregistrement des patients, le stockage des informations médicales, et la gestion des médecins et des infirmiers.

Création du code du programme
Crée un nouveau projet Java et implémente et teste le programme suivant :

## Hospital (point d'entrée du programme)

Méthode :


main(String[] args) : elle te servira à créer des instances des autres classes, pour tester que tout fonctionne correctement.




## Person (classe abstraite)

Attributs : name (String), age (int), socialSecurityNumber (String)
Méthodes :

getName()
getAge()
getSocialSecurityNumber()




## Medication (classe)

Attributs : name (String), dosage (String)
Méthodes :


getInfo() : retourne les informations sur le médicament




## Illness (classe)

Attributs : name (String), medicationList (ArrayList)
Méthodes :


addMedication(Medication medication) : ajoute un médicament à la liste

getInfo() : retourne les informations sur la maladie et la liste des informations des médicaments




## Patient (classe, hérite de Person)

Attributs : patientId (String), illnessList (ArrayList)
Méthodes :


addIllness(Illness illness) : ajoute une maladie à la liste du patient

getInfo() : retourne les informations du patient (nom, âge, numéro de sécurité sociale et les informations sur ses maladies)




## Care (interface)

 Méthodes :


void careForPatient(Patient patient) : pas d'implémentation par défaut

void recordPatientVisit(String notes) : implémentation par défaut qui affiche le contenu de la note




## MedicalStaff (classe abstraite, hérite de Person, implémente Care)

Attributs : employeeId (String)
Méthodes abstraites :

getRole()




## Doctor (classe, hérite de MedicalStaff)

Attributs : specialty (String)
Méthodes :


getRole() : affiche "Doctor"

careForPatient(Patient patient) : affiche "Doctor $doctorName cares for $patientName"




## Nurse (classe, hérite de MedicalStaff)

Méthodes :


getRole() : affiche "Nurse"

careForPatient(Patient patient) : affiche "Nurse $nurseName cares for $patientName"