public class Medication {

    private String name;
   private String dosage;

    public Medication(String nameOfMedication, String posologOfMedication) {
        this.name = nameOfMedication;
        this.dosage = posologOfMedication;
    }

    public String getMedicationInfos() {
        return this.name + ' ' + this.dosage;
    }
}
