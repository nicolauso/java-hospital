import java.util.ArrayList;

public class Illness {

    private String name;
    private ArrayList<Medication> medicationList = new ArrayList<>();

    public Illness(String pathology, ArrayList pharmacy) {
        this.name = pathology;
        this.medicationList = pharmacy;
    }

    public void addMedication(Medication medication) {
        medicationList.add(medication);
    }

    public String getIllnessInfo() {
        return this.name;
    }
}
