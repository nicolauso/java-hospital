import java.util.ArrayList;

public class Hopital {

    public static void main(String[] args) {

        // MEDICATION
        Medication remedaToutVa = new Medication("Remède panacée à base d'alcool", "1 à deux verres par jour");
        Medication foieDeBetterave = new Medication("Foie de betterave", "1 poil par jour");
        Medication rituelDuDesert = new Medication("Egrainer du sable", "12h par jour");
        // ILLNESS
        ArrayList<Medication> phamarcyListing = new ArrayList<>();
        phamarcyListing.add(rituelDuDesert);

        ArrayList<Medication> phamarcyListingTwo = new ArrayList<>();
        phamarcyListingTwo.add(rituelDuDesert);
        phamarcyListingTwo.add(foieDeBetterave);
        phamarcyListingTwo.add(remedaToutVa);


        Illness besoinDetreDansUneBoite = new Illness("Besoin irrépressible de vivre dans une boîte", phamarcyListing);
        Illness jaiMalMaisJeSaisPasOu = new Illness("Douleurs de la tête aux pieds", phamarcyListingTwo);
        Illness protuberanceLumineuse = new Illness("Protubérance lumineuse", phamarcyListing);


        ArrayList<Illness> toutesMesMaladies = new ArrayList<>();
        toutesMesMaladies.add(besoinDetreDansUneBoite);
        toutesMesMaladies.add(jaiMalMaisJeSaisPasOu);

        ArrayList<Illness> beaucoupDeMaladies = new ArrayList<>();
        beaucoupDeMaladies.add(besoinDetreDansUneBoite);
        beaucoupDeMaladies.add(protuberanceLumineuse);


        // TO DO : FAIRE LE GET INFOS DE DISEASES POUR AFFICHER AUTRE CHOSE QUE LES ADRESSES MEMOIRE
        String allNamesOfDiseases = "";
        for (Illness illness : beaucoupDeMaladies) {
            allNamesOfDiseases += illness.getIllnessInfo() + ", ";
        }
        String allMedications = "";
        for (Medication medication : phamarcyListingTwo) {
            allMedications += medication.getMedicationInfos() + ", ";
        }


        // DOCTOR
        Doctor doctorLove = new Doctor("Love problem solver", "Docteur Mamour", 35, "2845698301", "00215");
        Doctor doctorDeLaLepre = new Doctor("Lèpre", "Docteur Friable", 35, "2845697501", "00216");
        // NURSE
        Nurse nurseGastro = new Nurse("Léopold", 68, "16358469514", "0236");
        Nurse nurseCancero = new Nurse("Omar", 21, "784692036", "0260");
        // PATIENT
        Patient jattendsSvpJaiMal = new Patient("PaulQuiAMal", 100, "129852620", "520", toutesMesMaladies);

        Patient jeje = new Patient("Gérard", 12, "45951", "0265", beaucoupDeMaladies);

        //System.out.println("Le patient " + jeje.name + " souffre de ces maladies : " + allNamesOfDiseases);

        System.out.println("Pour soigner " + jeje.name + " de ses maladies, à savoir : " + allNamesOfDiseases + "l'infirmière " + nurseGastro.name + " se voit contrainte de lui administrer les remèdes suivants : " + allMedications + "et ça fait beaucoup.");


    }
}
