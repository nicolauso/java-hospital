public abstract class MedicalStaff extends Person implements Care {

   protected String employeeId;
    protected String role;
    public MedicalStaff(String name, int age, String socialSecurityNumber, String employeeId) {
        super(name, age, socialSecurityNumber);
        this.employeeId = employeeId;
    }

    public String getRole() {
        return this.role;
    }
}
