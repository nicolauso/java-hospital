public abstract class Person {

    protected String name;
    protected int age;
    protected String socialSecurityNumber;

    public Person(String name, int age, String socialSecurityNumber){
        this.name = name;
        this.age = age;
        this.socialSecurityNumber = socialSecurityNumber;
    }

    public String getName() {
        return this.name;
    }

    public int getAge() {
        return this.age;
    }

    public String getSocialSecurityNumber() {
        return this.socialSecurityNumber;
    }
}
