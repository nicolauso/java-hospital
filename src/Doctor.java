public class Doctor extends MedicalStaff {

    String specialty;

    public Doctor(String specialty, String name, int age, String socialSecurityNumber, String employeeId) {
        super(name, age, socialSecurityNumber, employeeId);
        this.specialty = specialty;
    }

    public void careForPatient(Patient patient) {
        System.out.println("Doctor" + ' ' +  this.name + ' ' + "cares for" + ' ' + patient.getName());
    }
}
