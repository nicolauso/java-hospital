import java.util.ArrayList;

public class Patient extends Person {

    String patientId;
    ArrayList<Illness> illnessList = new ArrayList<>();

    public Patient(String name, int age, String socialSecurityNumber, String patientId, ArrayList<Illness> illnessList) {
        super(name, age, socialSecurityNumber);
        this.patientId = patientId;
        this.illnessList = illnessList;
    }

    public void addIllness(Illness illness) {
        illnessList.add(illness);
    }

    public String getInfos() {
        ArrayList<String> allIllnessForThisPatient = new ArrayList<>();
        for (Illness illness : illnessList) {
            allIllnessForThisPatient.add(illness.getIllnessInfo());
        }
        return this.patientId + ' ' + getName() + ' ' + getAge() + ' ' + getSocialSecurityNumber() + allIllnessForThisPatient;
    }
}
